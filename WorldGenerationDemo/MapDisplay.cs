﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerationDemo
{
    //Note Not Really Used Since Endless Terrain Creates Chunks Dynamically
    class MapDisplay
    {
        //Plane To Display
        private Plane m_Plane;

        public MapDisplay(Plane plane)
        {
            m_Plane = plane;
        }

        public void DrawMesh(MeshData meshData, Texture2D texture)
        {
            //Draw Mesh Onto Plane
            Mesh mesh = meshData.CreateMesh();
            //m_Plane.SetVerticies(mesh.Verticies, mesh.UVs);
            //m_Plane.SetIndicies(mesh.Indicies);
            m_Plane.SetTexture(texture);
        }
    }
}
