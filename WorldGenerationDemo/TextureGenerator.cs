﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerationDemo
{
    class TextureGenerator
    {

        private static GraphicsDevice m_GraphicsDevice;

        public static void Initialize(GraphicsDevice graphicsDevice)
        {
            m_GraphicsDevice = graphicsDevice;
        }

        public static Texture2D TextureFromHeightMap(float[,] heightMap)
        {
            //Create Noise Texture
            int width = heightMap.GetLength(0);
            int height = heightMap.GetLength(1);
            Color[] colorMap = new Color[width * height];
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    colorMap[y * width + x] = Color.Lerp(Color.Black, Color.White, heightMap[x, y]);
                }
            }
            return TextureFromColorMap(colorMap, width, height);
        }

        public static Texture2D TextureFromColorMap(Color[] colorMap, int width, int height)
        {
            Texture2D texture = new Texture2D(m_GraphicsDevice,width,height);
            texture.SetData(colorMap);
            return texture;
        }
    }
}
