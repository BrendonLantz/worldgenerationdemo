﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerationDemo
{
    class MeshGenerator
    {

        public static MeshData GenerateFlatTerrainMesh(int width, int height)
        {
            //Create A Flat Terrain Mesh
            float[,] heightMap = new float[width,height];
            for(int y = 0; y < height; y++)
            {
                for(int x = 0; x < width; x++)
                {
                    heightMap[x, y] = 0.0f;
                }
            }
            return GenerateTerrainMesh(heightMap,1.0f,0);
        }

        public static MeshData GenerateTerrainMesh(float[,] heightMap, float heightMultiplier, int lod)
        {
            int width = heightMap.GetLength(0);
            int height = heightMap.GetLength(1);
            //Offset Mesh So It's Centered On Z
            float topLeftX = (width - 1) / -2f;
            float topLeftZ = (height - 1) / 2f;

            //Calculate The Simplification Increment
            int meshSimplificationIncrement = (lod == 0) ? 1 : lod * 2;

            //Vertex Count With Resepect To LOD
            int verticiesPerLine = ((width - 1) / meshSimplificationIncrement) + 1;

            MeshData meshData = new MeshData(verticiesPerLine,verticiesPerLine);
            int vertexIndex = 0;

            for(int y = 0; y < height; y+= meshSimplificationIncrement)
            {
                for(int x = 0; x < width; x+= meshSimplificationIncrement)
                {
                    //Add Vertex Data
                    meshData.verticies[vertexIndex] = new Vector3(topLeftX + x, heightMap[x, y] * heightMultiplier, topLeftZ - y);
                    meshData.uvs[vertexIndex] = new Vector2(x / (float)width, y / (float)height);
                    //If Not Outermost Verticies Construct Triangles
                    if (x < width - 1 && y < height - 1)
                    {
                        meshData.AddTriangle(vertexIndex, vertexIndex + verticiesPerLine + 1, vertexIndex + verticiesPerLine);
                        meshData.AddTriangle(vertexIndex + verticiesPerLine + 1, vertexIndex, vertexIndex + 1);
                    }
                    vertexIndex++;
                }
            }

            return meshData;
        }
    }

    public class MeshData
    {
        public Vector3[] verticies;
        public int[] triangles;
        public Vector2[] uvs;

        private int triangleIndex;

        public MeshData(int width, int height)
        {
            verticies = new Vector3[width * height];
            uvs = new Vector2[width * height];
            triangles = new int[(width - 1) * (height - 1) * 6];
        }

        public void AddTriangle(int a, int b, int c)
        {
            triangles[triangleIndex] = a;
            triangles[triangleIndex+1] = b;
            triangles[triangleIndex+2] = c;
            triangleIndex += 3;
        }

        public Mesh CreateMesh()
        {
            Mesh mesh = new Mesh();
            mesh.Verticies = new VertexPositionTexture[verticies.Length];
            for (int i = 0; i < mesh.Verticies.Length; i++)
            {
                mesh.Verticies[i].Position = verticies[i];
                mesh.Verticies[i].TextureCoordinate = uvs[i];
            }
            mesh.Indicies = triangles;
            return mesh;
        }
    }

    public class Mesh
    {
        public VertexPositionTexture[] Verticies;
        public int[] Indicies;
    }
}
