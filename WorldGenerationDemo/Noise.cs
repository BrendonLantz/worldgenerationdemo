﻿using Microsoft.Xna.Framework;
using SharpNoise;
using SharpNoise.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerationDemo
{
    class Noise
    {
        //Generate a noise map with values from 0 to 1
        public static float[,] GenerateNoiseMap(int mapWidth, int mapHeight, int seed, float scale, int octaves, float persistence, float lacunarity, Vector2 offset)
        {
            float[,] noiseMap = new float[mapWidth, mapHeight];

            float halfWidth = mapWidth / 2.0f;
            float halfHeight = mapHeight / 2.0f;

            //Create Perlin Noise
            Perlin perlin = new Perlin();
            perlin.Quality = NoiseQuality.Fast;
            perlin.OctaveCount = octaves;
            perlin.Persistence = persistence;
            perlin.Lacunarity = lacunarity;
            perlin.Seed = seed;

            float maxPossibleHeight = 2.0f;

            //Generate Noise
            for(int y = 0; y < mapHeight; y++)
            {
                for(int x = 0; x < mapWidth; x++)
                {
                    //Ensure Scaled Sample Is Centered
                    float sampleX = (x-halfWidth+offset.X) / scale;
                    float sampleY = (y-halfHeight-offset.Y) / scale;

                    //Get Perlin Value
                    float perlinValue = (float)perlin.GetValue(sampleX, sampleY, 0) * 2 - 1;

                    //Get Noise Height
                    float noiseHeight = perlinValue;

                    //Assign Map Value
                    noiseMap[x, y] = noiseHeight;
                }
            }

            //Normalize Noise
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    float normalizedHeight = (noiseMap[x, y] +1) / (2.0f * maxPossibleHeight);
                    noiseMap[x, y] = normalizedHeight;
                }
            }

            return noiseMap;
        }
    }
}
