﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WorldGenerationDemo
{
    class EndlessTerrain
    {
        private GraphicsDevice m_Device;
        public static MapGenerator m_MapGenerator;
        private Camera m_Camera;

        //Position Of The Observer
        private Vector2 m_Position;
        //Position Of The Observer Last Update
        private Vector2 m_PreviousPosition;
        //Size Of Terrain Chunk
        private int m_ChunkSize;
        //Number Of Chunks In View Distance
        private int m_ChunksVisible;
        //Distance Needed To Move Before A Chunk Update
        private float m_MoveThreshold = 25.0f;
        private float m_MoveThresholdSquared;

        //LODS And Their Distances
        private LODInfo[] m_DetailLevels = {
            new LODInfo(0,400),
            new LODInfo(0,800),
            new LODInfo(4,1200),
            new LODInfo(4,1600),
            new LODInfo(6,2000)
        };
        //Distance You Can See Terrain
        public static float m_MaxViewDist;

        //Map Of Chunks Based On Location
        private Dictionary<Vector2, TerrainChunk> m_Chunks;
        //List Of Previously Visible Chunks
        static List<TerrainChunk> m_PreviousVisibleChunks;


        public EndlessTerrain(GraphicsDeviceManager graphics, MapGenerator mapGenerator, Camera camera)
        {
            m_Device = graphics.GraphicsDevice;
            m_MapGenerator = mapGenerator;
            m_Camera = camera;
            m_Position = new Vector2(m_Camera.GetPosition().X, m_Camera.GetPosition().Z);
            m_MaxViewDist = m_DetailLevels[m_DetailLevels.Length-1].Range;
            m_MoveThresholdSquared = m_MoveThreshold * m_MoveThreshold;
            m_ChunkSize = mapGenerator.ChunkSize - 1;
            m_ChunksVisible = (int)(m_MaxViewDist / m_ChunkSize);
            m_Chunks = new Dictionary<Vector2, TerrainChunk>();
            m_PreviousVisibleChunks = new List<TerrainChunk>();

            //Ensure That Initial Chunks Are Updated
            UpdateVisibleChunks();
        }

        public List<TerrainChunk> GetChunks()
        {
            return m_Chunks.Values.ToList();
        }

        public void Update()
        {
            //Update Position
            m_Position = new Vector2(m_Camera.GetPosition().X,m_Camera.GetPosition().Z);
            //Update All Chunks
            if ((Vector2.Subtract(m_PreviousPosition,m_Position).LengthSquared() > m_MoveThresholdSquared))
            {
                m_PreviousPosition = m_Position;
                UpdateVisibleChunks();
            }
        }

        private void UpdateVisibleChunks()
        {
            //Set Old Chunks To Be Hidden
            for(int i = 0; i < m_PreviousVisibleChunks.Count; i++)
            {
                m_PreviousVisibleChunks[i].SetVisible(false);
            }
            m_PreviousVisibleChunks.Clear();

            //Position Of Chunk
            int currentChunkCoordX = (int)(m_Position.X / m_ChunkSize);
            int currentChunkCoordY = (int)(m_Position.Y / m_ChunkSize);
            for(int yOffset = -m_ChunksVisible; yOffset <= m_ChunksVisible; yOffset++)
            {
                for(int xOffset = -m_ChunksVisible; xOffset <= m_ChunksVisible; xOffset++)
                {
                    Vector2 viewedChunkCoord = new Vector2(currentChunkCoordX + xOffset, currentChunkCoordY + yOffset);
                    //If Chunk Exists Update
                    if (m_Chunks.ContainsKey(viewedChunkCoord))
                    {
                        m_Chunks[viewedChunkCoord].Update();
                    }
                    //Else Add New Chunk
                    else
                    {
                        m_Chunks.Add(viewedChunkCoord, new TerrainChunk(viewedChunkCoord,m_ChunkSize, m_DetailLevels, m_Camera, m_Device,m_MapGenerator));
                    }
                }
            }
        }

        public class TerrainChunk
        {
            //Plane Object To Render
            private Plane m_Plane;
            //Camera To Compute Distance From Chunk
            private Camera m_Camera;
            //Position Of Chunk In XZ
            private Vector2 m_Position;
            //Whether This Chunk Is Visible
            private bool m_Visible;

            private LODInfo[] m_DetailLevels;
            private LODMesh[] m_LODMeshes;
            private MapData m_MapData;
            private bool m_MapDataRecieved;
            private int m_PreviousLOD;

            //Graphics Device For Creating Buffers
            private GraphicsDevice m_Device;
            //Map Generator To Request Data
            private MapGenerator m_MapGenerator;

            public TerrainChunk(Vector2 coord, int size, LODInfo[] detailLevels, Camera camera, GraphicsDevice graphicsDevice, MapGenerator mapGenerator)
            {
                m_Position = coord * size;
                m_Camera = camera;
                m_DetailLevels = detailLevels;
                m_PreviousLOD = -1;
                m_Device = graphicsDevice;
                m_MapGenerator = mapGenerator;
                m_Plane = new Plane(new Vector3(m_Position.X, 0, m_Position.Y));
                SetVisible(false);

                //Create Lod Meshes
                m_LODMeshes = new LODMesh[m_DetailLevels.Length];

                for(int i = 0; i < m_DetailLevels.Length; i++)
                {
                    m_LODMeshes[i] = new LODMesh(m_DetailLevels[i].LOD, Update);
                }

                m_MapGenerator.RequestMapData(m_Position, OnMapDataReceived);
            }

            public void Dispose()
            {
                m_Plane.DisposeBuffers();
            }

            public Plane GetPlane()
            {
                return m_Plane;
            }

            void OnMapDataReceived(MapData mapData)
            {
                m_MapData = mapData;
                m_MapDataRecieved = true;

                //Load Texture
                Texture2D texture = TextureGenerator.TextureFromColorMap(mapData.ColorMap, m_MapGenerator.ChunkSize, m_MapGenerator.ChunkSize);
                m_Plane.SetTexture(texture);

                Update();
            }

            public void Update()
            {
                if (m_MapDataRecieved)
                {
                    float distance = (float)Math.Sqrt(Vector3.DistanceSquared(new Vector3(m_Position.X,0,m_Position.Y),m_Camera.GetPosition()));
                    bool visible = distance <= m_MaxViewDist;
                    if (visible)
                    {
                        //Determine LOD Level
                        int lodIndex = 0;
                        for (int i = 0; i < m_DetailLevels.Length - 1; i++)
                        {
                            if (distance > m_DetailLevels[i].Range)
                            {
                                lodIndex = i + 1;
                            }
                            else
                            {
                                break;
                            }
                        }

                        if (lodIndex != m_PreviousLOD)
                        {
                            LODMesh lodMesh = m_LODMeshes[lodIndex];
                            if (lodMesh.hasMesh)
                            {
                                //Load Mesh
                                m_PreviousLOD = lodIndex;
                                if (!m_Plane.HasLOD(lodMesh))
                                {
                                    m_Plane.AddLOD(lodMesh, m_Device);
                                }
                                m_Plane.SetLOD(lodMesh);
                            }
                            else if (!lodMesh.hasRequestedMesh)
                            {
                                lodMesh.RequestMesh(m_MapData);
                            }
                        }
                        m_PreviousVisibleChunks.Add(this);
                    }
                    else
                    {
                        //m_Plane.DisposeBuffers();
                    }
                    SetVisible(visible);
                }
            }

            public bool IsVisible()
            {
                return m_Visible;
            }

            public void SetVisible(bool visible)
            {
                m_Visible = visible;
            }
        }

        public class LODMesh
        {
            public Mesh mesh;
            public bool hasRequestedMesh;
            public bool hasMesh;
            public int lod;
            private Action m_UpdateCallback;

            public LODMesh(int lod, Action updateCallback)
            {
                this.lod = lod;
                m_UpdateCallback = updateCallback;
            }

            private void OnMeshDataRecieved(MeshData meshData)
            {
                mesh = meshData.CreateMesh();
                hasMesh = true;
                m_UpdateCallback();
            }

            public void RequestMesh(MapData mapData)
            {
                hasRequestedMesh = true;
                m_MapGenerator.RequestMeshData(mapData, lod, OnMeshDataRecieved);
            }

        }

        public struct LODInfo
        {
            //The Level Of Detail
            public int LOD;
            //The Range It Is Active
            public float Range;

            public LODInfo(int lod, float range)
            {
                LOD = lod;
                Range = range;
            }
        }
    }
}
