﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerationDemo
{
    class Camera
    {

        //Orientation
        private Vector3 m_Position;
        private Vector3 m_View;
        private Vector3 m_Up;

        //Camera Angle
        private float m_Yaw = 0;
        private float m_Pitch = 0;

        //View And Projection Matricies
        private Matrix m_ProjectionMatrix; 
        private Matrix m_ViewMatrix;

        //Projection Settings
        private float m_FOV=MathHelper.PiOver4;
        private float m_AspectRatio=1.77f;
        private float m_NearClip=0.1f;
        private float m_FarClip=10000.0f;

        //Speed
        private float m_Speed = 3f;
        private float m_TurnSpeed = 50.0f;

        //Sensitivity
        private float m_MouseSensitivityX = 1f;
        private float m_MouseSensitivityY = 1f;

        //Mouse
        private MouseState m_Previous = Mouse.GetState();

        //Movement Vector
        private Vector3 m_Movement;

        public Camera(Vector3 position, Vector3 target, Vector3 up)
        {
            m_Position = position;
            m_View = target;
            m_Up = up;

            //Create Matricies
            UpdateProjectionMatrix();
            UpdateViewMatrix();
        }

        public Vector3 GetPosition()
        {
            return m_Position;
        }

        public Matrix GetViewMatrix()
        {
            return m_ViewMatrix;
        }

        public Matrix GetProjectionMatrix()
        {
            return m_ProjectionMatrix;
        }

        public void Rotate(float DeltaX, float DeltaY)
        {
            //Rotate Camera
            m_Yaw -= (MathHelper.ToRadians(DeltaY) * m_MouseSensitivityY);
            m_Pitch -= (MathHelper.ToRadians(DeltaX) * m_MouseSensitivityX);
        }

        public void Move(float delta)
        {
            //Update Keyboard Movement
            m_Movement = Vector3.Zero;
            if (Keyboard.GetState().IsKeyDown(Keys.W))
            {
                m_Movement += Vector3.Forward;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                m_Movement += Vector3.Left;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                m_Movement += Vector3.Backward;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                m_Movement += Vector3.Right;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Q))
            {
                m_Pitch -= (MathHelper.ToRadians(m_TurnSpeed*delta));
            }
            if (Keyboard.GetState().IsKeyDown(Keys.E))
            {
                m_Pitch += (MathHelper.ToRadians(m_TurnSpeed*delta));
            }
        }

        public void Update()
        {
            //Update Position And View
            Matrix rotX = Matrix.CreateRotationX(m_Yaw);
            Matrix rotY = Matrix.CreateRotationY(m_Pitch);
            Matrix rotation = rotX * rotY;
            Vector3 rotatedMovement = Vector3.Transform(m_Movement, rotation);
            m_Position += rotatedMovement * m_Speed;

            //Update View Matrix
            UpdateViewMatrix();
        }

        private void UpdateProjectionMatrix()
        {
            m_ProjectionMatrix = Matrix.CreatePerspectiveFieldOfView(m_FOV, m_AspectRatio, m_NearClip, m_FarClip);
        }

        private void UpdateViewMatrix()
        {
            //Create Rotation
            Matrix rotX = Matrix.CreateRotationX(m_Yaw);
            Matrix rotY = Matrix.CreateRotationY(m_Pitch);
            Matrix rotation = rotX * rotY;

            //Rotate View
            Vector3 originalTarget = Vector3.Forward;
            Vector3 rotatedTarget = Vector3.Transform(originalTarget, rotation);
            Vector3 finalTarget = m_Position + rotatedTarget;

            //Rotate Up Vector
            Vector3 originalUp = Vector3.Up;
            Vector3 rotatedUp = Vector3.Transform(originalUp, rotation);

            //Construct View Matrix
            m_ViewMatrix = Matrix.CreateLookAt(m_Position, finalTarget, rotatedUp);
        }
    }
}
