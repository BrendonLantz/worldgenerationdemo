﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace WorldGenerationDemo
{
    class MapGenerator
    {
        //Draw Modes
        public enum DrawMode
        {
            NoiseMap,
            ColorMap,
            Mesh
        }
        //Current Draw Mode
        public DrawMode drawMode=DrawMode.Mesh;

        //Size Of The Map (241 is divisible by with 1-12 so it's ideal for LOD)
        public int ChunkSize = 241;
        //Level Of Detail
        public int previewLOD=0;
        //Mesh Height Multiplier
        public float meshHeightMultiplier=80.0f;
        //Scale Of The Noise
        public float noiseScale=80.0f;
        //Number Of Noise Passes
        public int octaves=5;
        //Multiplier For Diminishing Amplitude Each Octave
        public float persistence=0.5f;
        //Multiplier For How Quickly The Frequency Increases Each Octave
        public float lacunarity=1.7f;
        //Seed For Random Map
        public int seed=0;
        //Offset Of Noise Map
        public Vector2 offset = new Vector2(0,0);
        //Regions
        public TerrainType[] regions =
        {
            new TerrainType("Water Deep",0.0f,new Color(0,51,255)),
            new TerrainType("Water Shallow",0.1f,new Color(46,86,255)),
            new TerrainType("Sand",0.2f,new Color(237,255,183)),
            new TerrainType("Grass",0.3f,new Color(82,244,46)),
            new TerrainType("Dark Grass",0.5f,new Color(29,160,0)),
            new TerrainType("Rock",0.6f,new Color(109,109,109)),
            new TerrainType("Dark Rock",0.7f,new Color(53,53,53)),
            new TerrainType("Snow",1.0f,new Color(255,255,255)),
        };

        private Queue<MapThreadInfo<MapData>> m_MapDataInfo;
        private Queue<MapThreadInfo<MeshData>> m_MeshDataInfo;

        //Class To Draw The Map
        private MapDisplay m_MapDisplay;

        public MapGenerator()
        {
            m_MapDisplay = new MapDisplay(new Plane());
            m_MapDataInfo = new Queue<MapThreadInfo<MapData>>();
            m_MeshDataInfo = new Queue<MapThreadInfo<MeshData>>();
        }

        //Generates A New Noise Map
        private MapData GenerateMapData(Vector2 center)
        {
            //Validate Input
            Validate();

            //Create Noise Map
            float[,] noiseMap = Noise.GenerateNoiseMap(ChunkSize, ChunkSize, seed, noiseScale, octaves, persistence, lacunarity, center + offset);

            //Create Color Map
            Color[] colorMap = new Color[ChunkSize * ChunkSize];
            for(int y = 0; y < ChunkSize; y++)
            {
                for(int x = 0; x < ChunkSize; x++)
                {
                    float currentHeight = noiseMap[x, y];
                    for(int i = 0; i < regions.Length; i++)
                    {
                        if (currentHeight <= regions[i].Height)
                        {
                            colorMap[y * ChunkSize + x] = regions[i].Color;
                            break;
                        }
                    }
                }
            }
            return new MapData(noiseMap, colorMap);
        }

        public void DrawMap()
        {
            MapData mapData = GenerateMapData(Vector2.Zero);
            //Draw Map
            if (drawMode == DrawMode.NoiseMap)
            {
                m_MapDisplay.DrawMesh(MeshGenerator.GenerateFlatTerrainMesh(ChunkSize,ChunkSize), TextureGenerator.TextureFromHeightMap(mapData.HeightMap));
            }
            if (drawMode == DrawMode.ColorMap)
            {
                m_MapDisplay.DrawMesh(MeshGenerator.GenerateFlatTerrainMesh(ChunkSize,ChunkSize), TextureGenerator.TextureFromColorMap(mapData.ColorMap,ChunkSize,ChunkSize));
            }
            if (drawMode == DrawMode.Mesh)
            {
                m_MapDisplay.DrawMesh(MeshGenerator.GenerateTerrainMesh(mapData.HeightMap, meshHeightMultiplier, previewLOD), TextureGenerator.TextureFromColorMap(mapData.ColorMap, ChunkSize, ChunkSize));
            }
        }

        public void RequestMapData(Vector2 center, Action<MapData> callback)
        {
            MapDataThread(center, callback);
        }

        void MapDataThread(Vector2 center, Action<MapData> callback)
        {
            MapData mapData = GenerateMapData(center);
            m_MapDataInfo.Enqueue(new MapThreadInfo<MapData>(callback, mapData));
        }

        public void RequestMeshData(MapData mapData, int lod, Action<MeshData> callback)
        {
            MeshDataThread(mapData, lod, callback);
        }

        void MeshDataThread(MapData mapData, int lod, Action<MeshData> callback)
        {
            MeshData meshData = MeshGenerator.GenerateTerrainMesh(mapData.HeightMap, meshHeightMultiplier, lod);
            m_MeshDataInfo.Enqueue(new MapThreadInfo<MeshData>(callback,meshData));
        }

        public void Update()
        {
            //Process Map Requests
            if (m_MapDataInfo.Count > 0)
            {
                for(int i = 0; i < m_MapDataInfo.Count; i++)
                {
                    MapThreadInfo<MapData> threadInfo = m_MapDataInfo.Dequeue();
                    threadInfo.Callback(threadInfo.Parameter);
                }
            }
            //Process Mesh Requests
            if (m_MeshDataInfo.Count > 0)
            {
                for(int i = 0; i < m_MeshDataInfo.Count; i++)
                {
                    MapThreadInfo<MeshData> threadInfo = m_MeshDataInfo.Dequeue();
                    threadInfo.Callback(threadInfo.Parameter);
                }
            }
        }

        public void Validate()
        {
            //Prevent scale from being zero
            if (noiseScale <= 0)
            {
                noiseScale = 0.0001f;
            }
            //Prevent Negative Or Zero Lacunarity
            if (lacunarity < 1)
            {
                lacunarity = 1;
            }
            //Prevent Negative Octaves
            if (octaves < 0)
            {
                octaves = 0;
            }
            //Check If Persistence Is Within Range [0,1]
            if (persistence < 0)
            {
                persistence = 0.0f;
            }
            if (persistence > 1)
            {
                persistence = 1.0f;
            }
        }

        struct MapThreadInfo<T>
        {
            public readonly Action<T> Callback;
            public readonly T Parameter;

            public MapThreadInfo(Action<T> callback, T parameter)
            {
                Callback = callback;
                Parameter = parameter;
            }
        }
    }
}

public struct TerrainType
{
    public string Name;
    public float Height;
    public Color Color;

    public TerrainType(string name, float height, Color color)
    {
        Name = name;
        Height = height;
        Color = color;
    }
}

public struct MapData
{
    public readonly float[,] HeightMap;
    public readonly Color[] ColorMap;

    public MapData(float[,] heightMap, Color[] colorMap)
    {
        HeightMap = heightMap;
        ColorMap = colorMap;
    }
}