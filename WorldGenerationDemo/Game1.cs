﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Configuration;
using static WorldGenerationDemo.EndlessTerrain;

namespace WorldGenerationDemo
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;
        BasicEffect effect;

        MapGenerator mapGenerator;
        EndlessTerrain endlessTerrain;
        Camera camera;

        bool wireFrame = true;
        bool updateTerrain = true;

        KeyboardState lastKeyboardState;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = Content.Load<SpriteFont>("Font");
            effect = new BasicEffect(GraphicsDevice);
            TextureGenerator.Initialize(GraphicsDevice);

            //Set Display Size
            int width = int.Parse(ConfigurationManager.AppSettings["Width"]);
            int height = int.Parse(ConfigurationManager.AppSettings["Height"]);
            bool Fullscreen = bool.Parse(ConfigurationManager.AppSettings["Fullscreen"]);
            bool VSync = bool.Parse(ConfigurationManager.AppSettings["VSync"]);

            graphics.PreferredBackBufferWidth = width;
            graphics.PreferredBackBufferHeight = height;
            graphics.SynchronizeWithVerticalRetrace = VSync;

            if (Fullscreen)
            {
                graphics.ToggleFullScreen();
            }
            graphics.ApplyChanges();

            //Set Rasterization Options
            if (wireFrame)
            {
                SetRasterizationState(CullMode.CullClockwiseFace, FillMode.WireFrame);
            }
            else
            {
                SetRasterizationState(CullMode.CullClockwiseFace, FillMode.Solid);
            }

            //Set Texture Sampling
            GraphicsDevice.SamplerStates[0] = SamplerState.AnisotropicClamp;
            graphics.PreferMultiSampling = true;

            //Center Mouse
            Vector2 screenCenter = new Vector2(GraphicsDevice.Viewport.Width / 2.0f, GraphicsDevice.Viewport.Height / 2.0f);
            Mouse.SetPosition((int)screenCenter.X, (int)screenCenter.Y);

            //Create A Camera And Plane
            camera = new Camera(new Vector3(0, 60, 60), new Vector3(0, 0, 0), Vector3.Up);
            mapGenerator = new MapGenerator();
            endlessTerrain = new EndlessTerrain(graphics, mapGenerator, camera);
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            //Check For Exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Exit();
            }

            //Toggle Wireframe
            if (IsKeyPressed(Keys.F1))
            {
                ToggleWireFrame();
            }

            //Toggle Terrain Update
            if (IsKeyPressed(Keys.F2))
            {
                if (updateTerrain)
                {
                    updateTerrain = false;
                }
                else
                {
                    updateTerrain = true;
                }
            }

            //Move Camera With Keyboard
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000.0f;
            camera.Move(deltaTime);

            //Update Map
            mapGenerator.Update();

            //Update Chunks
            if (updateTerrain)
            {
                endlessTerrain.Update();
            }

            //Update Camera
            camera.Update();

            //Update Last Keyboard State
            lastKeyboardState = Keyboard.GetState();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            //Clear Screen
            GraphicsDevice.Clear(Color.Black);

            //Render
            foreach (TerrainChunk terrainChunk in endlessTerrain.GetChunks())
            {
                //Only Render Chunks Within View Distance
                if (terrainChunk.IsVisible())
                {
                    //Check If Chunk Has Loaded
                    if (terrainChunk.GetPlane().IsLoaded())
                    {
                        RenderTerrainChunk(terrainChunk.GetPlane());
                    }
                }
            }

            base.Draw(gameTime);
        }

        private void RenderTerrainChunk(Plane plane)
        {
            //Load MVP Matricies
            effect.World = Matrix.CreateWorld(plane.GetPosition(), Vector3.Forward, Vector3.Up);
            effect.View = camera.GetViewMatrix();
            effect.Projection = camera.GetProjectionMatrix();

            //Apply Texture
            if (plane.GetTexture() != null)
            {
                effect.Texture = plane.GetTexture();
                effect.TextureEnabled = true;
            }

            //Bind Buffers
            GraphicsDevice.SetVertexBuffer(plane.GetVertexBuffer());
            GraphicsDevice.Indices = plane.GetIndexBuffer();

            //Render
            foreach (var pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, plane.GetIndicesLength());
            }
        }

        private void SetRasterizationState(CullMode cullMode, FillMode fillMode)
        {
            //Set Graphics Device CullMode And FillMode States
            RasterizerState rasterizerState = new RasterizerState();
            rasterizerState.FillMode = fillMode;
            rasterizerState.CullMode = cullMode;
            rasterizerState.MultiSampleAntiAlias = true;
            GraphicsDevice.RasterizerState = rasterizerState;
        }

        private void ToggleWireFrame()
        {
            if (wireFrame)
            {
                SetRasterizationState(CullMode.CullClockwiseFace, FillMode.Solid);
                wireFrame = false;
            }
            else
            {
                SetRasterizationState(CullMode.CullClockwiseFace, FillMode.WireFrame);
                wireFrame = true;
            }
        }

        private bool IsKeyPressed(Keys key)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            if ((keyboardState.IsKeyDown(key)) && (lastKeyboardState.IsKeyUp(key)))
            {
                return true;
            }
            return false;
        }
    }
}
