﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using static WorldGenerationDemo.EndlessTerrain;

namespace WorldGenerationDemo
{
    class Plane
    {
        //Plane Position
        private Vector3 m_Position;
        //Plane Texture
        private Texture2D m_Texture;
        //Plane Vertex Buffers
        private Dictionary<int, VertexBuffer> m_VertexBuffers;
        //Plane Index Buffers
        private Dictionary<int, IndexBuffer> m_IndexBuffers;
        //LOD
        private int m_LOD;

        //Bool For When Buffers Are Created
        private bool m_Loaded;

        public Plane()
        {
            m_Position = new Vector3(0, 0, 0);
            m_Loaded = false;
        }

        public Plane(Vector3 position)
        {
            m_Position = position;
            m_Loaded = false;
            m_VertexBuffers = new Dictionary<int, VertexBuffer>();
            m_IndexBuffers = new Dictionary<int, IndexBuffer>();
        }

        public Vector3 GetPosition()
        {
            return m_Position;
        }

        public int GetIndicesLength()
        {
            return m_IndexBuffers[m_LOD].IndexCount;
        }

        public Texture2D GetTexture()
        {
            return m_Texture;
        }

        public void SetTexture(Texture2D texture)
        {
            m_Texture = texture;
        }

        public VertexBuffer GetVertexBuffer()
        {
            return m_VertexBuffers[m_LOD];
        }

        public IndexBuffer GetIndexBuffer()
        {
            return m_IndexBuffers[m_LOD];
        }

        public bool HasLOD(LODMesh lod)
        {
            foreach (KeyValuePair<int,VertexBuffer> pair in m_VertexBuffers)
            {
                if (pair.Key == lod.lod)
                {
                    return true;
                }
            }
            return false;
        }

        public void AddLOD(LODMesh lod, GraphicsDevice graphicsDevice)
        {
            m_VertexBuffers[lod.lod] = (new VertexBuffer(graphicsDevice, typeof(VertexPositionTexture), lod.mesh.Verticies.Length, BufferUsage.None));
            m_VertexBuffers[lod.lod].SetData(lod.mesh.Verticies,0,lod.mesh.Verticies.Length);
            m_IndexBuffers[lod.lod] = (new IndexBuffer(graphicsDevice, IndexElementSize.ThirtyTwoBits, lod.mesh.Indicies.Length, BufferUsage.None));
            m_IndexBuffers[lod.lod].SetData(lod.mesh.Indicies, 0, lod.mesh.Indicies.Length);
        }

        public void SetLOD(LODMesh lod)
        {
            m_LOD = lod.lod;
            m_Loaded = true;
        }

        public void DisposeBuffers()
        {
            foreach(KeyValuePair<int,VertexBuffer> pair in m_VertexBuffers)
            {
                if (!pair.Value.IsDisposed)
                {
                    pair.Value.Dispose();
                }
            }
            foreach(KeyValuePair<int,IndexBuffer> pair in m_IndexBuffers)
            {
                if (pair.Value.IsDisposed)
                {
                    pair.Value.Dispose();
                }
            }
            if (!m_Texture.IsDisposed)
            {
                m_Texture.Dispose();
            }
            m_Loaded = false;
        }

        public bool IsLoaded()
        {
            return m_Loaded;
        }
    }
}
