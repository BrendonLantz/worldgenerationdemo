Introduction:
This is a demo project for Terrain Generation based on Sebastian Lague's Procedural Landmass Generation Youtube Series. It features mesh generation based on perlin noise, texture generation from height values, level of detail, and dynamic terrain chunk loading. This project uses Monogame a C# gaming framework based on XNA.

Build Instructions:
This project depends on .Net, SharpNoise, and Monogame. Nuget should install the packages needed when you build in Visual Studio. You should be able to clone the project, and build with visual studio without problems.

Notes
- ASDW moves the camera
- Q rotates left
- E rotates right
- F1 toggles wireframe
- F2 toggles terrain chunk updates
- The appconfig has the settings for display resolution, fullscreen, and VSync
- There are many settings in the MapGenerator class that you can alter to get different landscape effects.
